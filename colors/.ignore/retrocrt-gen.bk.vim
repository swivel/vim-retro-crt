" =============================================================================
" Name: vim-amber
" Amber theme for Vim
" -----------------------------------------------------------------------------
"
" This is a theme that simulates the retro PC amber monochrome CRT. It has
" pretty much no syntax-specific variations in color and is, instead, focusing
" on providing variations for the editor elements themselves (e.g., line
" numbers, color column, etc).
"
" This theme can toggle between light and dark mode by setting the `background`
" variable to "light" or "dark".
"
" Some tricks and code borrowed from Lucious theme by Jonathan Filip.
" =============================================================================

hi clear
hi clear Normal
if exists('syntax on')
	syntax reset
endif

let colors_name='retrocrt'

set background=dark

let s:fg1 = "#fc9505"
let s:fg2 = "#e58806"
let s:fg3 = "#db7d02"
let s:fg4 = "#9e5d07"

let s:bg1 = "#140b05"
let s:bg2 = "#1c1008"
let s:bg3 = "#261b0d"
let s:bg4 = "#c56306"

let s:bg = [s:bg1, 235, "black"]

" Darker background shades
let s:bgd_1 = [s:bg2, 234, "black"]

" Lighter background shades
let s:bgl_1 = [s:bg3, 236, "darkgray"]
let s:bgl_2 = [s:bg4, 237, "darkgray"]
let s:bgl_3 = [s:fg4, 238, "darkgray"]
let s:bgl_4 = [s:fg3, 241, "darkgray"]

" Foreground
let s:fg = [s:fg1, 251, "white"]

" Accents
let s:purple = ["#af87d7", 140, "darkmagenta"]
let s:orange = ["#d75f5f", 167, "red"]
let s:grey = ["#767676", 243, "darkgray"]
let s:green = ["#87af87", 108, "darkgreen"]
let s:red = ["#af5f87", 132, "darkred"]
let s:yellow = ["#d7af5f", 179, "darkyellow"]


let s:highlights = [
			\ [ "Normal", s:bg, s:fg, "NONE" ],
			\ [ "NonText", "bg", s:bgl_3, "NONE" ],
			\ [ "Comment", "bg", s:bgl_4, "NONE" ],
			\ [ "Conceal", "bg", s:bgl_4, "NONE" ],
			\ 
			\ [ "Constant", "bg", s:purple, "NONE" ],
			\ [ "Character", "Constant" ],
			\ [ "Number", "Constant" ],
			\ [ "Float", "Number" ],
			\ [ "Boolean", "Constant" ],
			\ [ "String", "Constant" ],
			\ 
			\ [ "Identifier", "bg", s:fg, "NONE" ],
			\ [ "Function", "Identifier" ],
			\ 
			\ [ "Statement", "bg", s:grey, "NONE" ],
			\ [ "Conditonal", "Statement" ],
			\ [ "Repeat", "Statement" ],
			\ [ "Label", "Statement" ],
			\ [ "Keyword", "Statement" ],
			\ [ "Exception", "Statement" ],
			\ 
			\ [ "Operator", "bg", s:fg, "NONE" ],
			\ 
			\ [ "PreProc", "bg", s:grey, "NONE" ],
			\ [ "Include", "PreProc" ],
			\ [ "Define", "PreProc" ],
			\ [ "Macro", "PreProc" ],
			\ [ "PreCondit", "PreProc" ],
			\ 
			\ [ "Type", "bg", s:fg, "NONE" ],
			\ [ "StorageClass", "Type" ],
			\ [ "Structure", "Type" ],
			\ [ "Typedef", "Type" ],
			\ 
			\ [ "Special", "NONE", s:grey, "NONE" ],
			\ [ "SpecialChar", "Special" ],
			\ [ "Tag", "Special" ],
			\ [ "Delimiter", "Special" ],
			\ [ "SpecialComment", "Special" ],
			\ [ "Debug", "Special" ],
			\ 
			\ [ "Error", "NONE", s:red, "NONE" ],
			\ [ "ErrorMsg", "Error" ],
			\ [ "Warning", "NONE", s:yellow, "NONE" ],
			\ [ "WarningMsg", "Warning" ],
			\ 
			\ [ "ModeMsg", "NONE", s:grey, "NONE" ],
			\ [ "MoreMsg", "ModeMsg" ],
			\ [ "Question", "ModeMsg" ],
			\ 
			\ [ "Ignore", "NonText" ],
			\ [ "Todo", "NONE", s:orange, "bold" ],
			\ [ "Underlined", "NONE", s:fg, "underline" ],
			\ 
			\ [ "StatusLine", s:bgl_2, s:purple, "NONE" ],
			\ [ "StatusLineNC", s:bgl_1, s:grey, "NONE" ],
			\ [ "StatusLineTerm", "StatusLine" ],
			\ [ "StatusLineTermNC", "StatusLineNC" ],
			\ [ "TabLine", "StatusLineNC" ],
			\ [ "TabLineFill", "StatusLineNC" ],
			\ [ "TabLineSel", "StatusLine" ],
			\ [ "WildMenu", s:bgl_1, s:orange, "NONE" ],
			\ [ "VertSplit", s:bgl_1, s:bgl_1, "NONE" ],
			\ 
			\ [ "Title", "NONE", s:fg, "bold" ],
			\ 
			\ [ "LineNr", "NONE", s:bgl_4, "NONE" ],
			\ [ "CursorLineNr", s:bgl_1, s:purple, "NONE" ],
			\ [ "Cursor", s:purple, s:fg, "NONE" ],
			\ [ "CursorLine", s:bgl_1, "NONE", "NONE" ],
			\ [ "CursorColumn", "CursorLine" ],
			\ [ "ColorColumn", s:bgd_1, "NONE", "NONE" ],
			\ [ "SignColumn", "NONE", s:grey, "NONE" ],
			\ 
			\ [ "Visual", s:bgl_2, "NONE", "NONE" ],
			\ [ "VisualNOS", s:bgl_3, "NONE", "NONE" ],
			\ 
			\ [ "Pmenu", s:bgl_2, "NONE", "NONE" ],
			\ [ "PmenuSbar", s:bgl_1, "NONE", "NONE" ],
			\ [ "PmenuSel", s:bgl_1, s:purple, "NONE" ],
			\ [ "PmenuThumb", s:orange, "NONE", "NONE" ],
			\ 
			\ [ "FoldColumn", "NONE", s:bgl_4, "NONE" ],
			\ [ "Folded", s:bgd_1, s:grey, "NONE" ],
			\ 
			\ [ "SpecialKey", "NONE", s:grey, "NONE" ],
			\ [ "IncSearch", s:orange, s:bg, "NONE" ],
			\ [ "Search", s:purple, s:bg, "NONE" ],
			\ [ "Directory", "NONE", s:purple, "NONE" ],
			\ [ "MatchParen", "NONE", s:orange, "bold" ],
			\ 
			\ [ "SpellBad", "NONE", s:red, "underline" ],
			\ [ "SpellCap", "NONE", s:green, "underline" ],
			\ [ "SpellLocal", "NONE", s:yellow, "underline" ],
			\ [ "SpellRare", "SpellLocal" ],
			\ 
			\ [ "QuickFixLine", s:bgd_1, "NONE", "NONE" ],
			\ 
			\ [ "DiffAdd", s:bgl_1, s:green, "NONE" ],
			\ [ "DiffChange", s:bgl_1, "NONE", "NONE" ],
			\ [ "DiffDelete", s:bgl_1, s:red, "NONE" ],
			\ [ "DiffText", s:bgl_1, s:yellow, "NONE" ],
			\ [ "diffAdded", "DiffAdd" ],
			\ [ "diffRemoved", "DiffDelete" ],
			\ 
			\ [ "helpHyperTextJump", "bg", s:purple, "NONE" ],
			\ [ "htmlTag", "htmlTagName" ],
			\ [ "htmlEndTag", "htmlTag" ],
			\ [ "gitcommitSummary", "Title" ]
			\ ]

" Dark
	" black
	" red
	" green
	" yellow
	" blue
	" magenta
	" cyan
	" white
" Bright
let s:terminal_ansi_colors = [
			\ s:bg,
			\ ["#ac2c2c"],
			\ ["#4e9a06"],
			\ ["#c4a000"],
			\ ["#3465a4"],
			\ ["#75507b"],
			\ ["#389aad"],
			\ s:bgl_4,
			\ 
			\ s:grey,
			\ s:red,
			\ s:green,
			\ s:yellow,
			\ ["#729fcf"],
			\ s:purple,
			\ ["#34e2e2"],
			\ s:fg
			\ ]

if ($TERM =~ '256' || &t_Co >= 256) || has("gui_running")
	for s:highlight in s:highlights
		if len(s:highlight) == 4
			exec "hi " . s:highlight[0] .
				\ " ctermbg=" . (type(s:highlight[1]) == type("") ? s:highlight[1] : s:highlight[1][1]) .
				\ " ctermfg=" . (type(s:highlight[2]) == type("") ? s:highlight[2] : s:highlight[2][1]) .
				\ " cterm=" . s:highlight[3] .
				\ " guibg=" . (type(s:highlight[1]) == type("") ? s:highlight[1] : s:highlight[1][0]) .
				\ " guifg=" . (type(s:highlight[2]) == type("") ? s:highlight[2] : s:highlight[2][0]) .
				\ " gui=" . s:highlight[3]
		elseif len(s:highlight) > 4
			exec "hi " . s:highlight[0] .
				\ " ctermbg=" . (type(s:highlight[1]) == type("") ? s:highlight[1] : s:highlight[1][1]) .
				\ " ctermfg=" . (type(s:highlight[2]) == type("") ? s:highlight[2] : s:highlight[2][1]) .
				\ " cterm=" . s:highlight[3] .
				\ " guibg=" . (type(s:highlight[1]) == type("") ? s:highlight[1] : s:highlight[1][0]) .
				\ " guifg=" . (type(s:highlight[2]) == type("") ? s:highlight[2] : s:highlight[2][0]) .
				\ " gui=" . s:highlight[3] .
				\ " guisp=" . (type(s:highlight[4]) == type("") ? s:highlight[4] : s:highlight[4][0])
		endif
	endfor

elseif &t_Co == 8 || $TERM !~# '^linux' || &t_Co == 16
	set t_Co=16

	for s:highlight in s:highlights
		if len(s:highlight) > 2
			exec "hi " . s:highlight[0] .
					\ " ctermbg=" . type(s:highlight[1]) == type("") ? s:highlight[1] : s:highlight[1][2] .
					\ " ctermfg=" . type(s:highlight[2]) == type("") ? s:highlight[2] : s:highlight[2][2] .
					\ " cterm=" . s:highlight[3]:
		endif
	endfor
endif

" Links
for s:link in s:highlights
	if len(s:link) == 2
		exec "hi! link " . s:link[0] . " " . s:link[1]
	endif
endfor

if len(s:terminal_ansi_colors) == 16
	let g:terminal_ansi_colors = s:terminal_ansi_colors
endif
