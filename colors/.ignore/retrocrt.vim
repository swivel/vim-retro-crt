" =============================================================================
" Name: vim-amber
" Amber theme for Vim
" -----------------------------------------------------------------------------
"
" This is a theme that simulates the retro PC amber monochrome CRT. It has
" pretty much no syntax-specific variations in color and is, instead, focusing
" on providing variations for the editor elements themselves (e.g., line
" numbers, color column, etc).
"
" This theme can toggle between light and dark mode by setting the `background`
" variable to "light" or "dark".
"
" Some tricks and code borrowed from Lucious theme by Jonathan Filip.
" =============================================================================

hi clear
if exists('syntax on')
	syntax reset
endif

let colors_name='retrocrt'

let s:style = &background

if s:style == "dark"
	let s:fg1 = "#fc9505"
	let s:fg2 = "#e58806"
	let s:fg3 = "#db7d02"
	let s:fg4 = "#9e5d07"

	let s:bg1 = "#140b05"
	let s:bg2 = "#1c1008"
	let s:bg3 = "#261b0d"
	let s:bg4 = "#c56306"
"else
"	let s:fg1 = "#140b05"
"	let s:bg1 = "#fc9505"
endif

hi clear Normal

" Cursor Colors
"	let s:synCursor = [
"				\ "Cursor", "Char under the active cursor
"				\ "CursorIM", "Cursor for i18n chars
"				\ "CursorColumn", "Current column of cursor
"				\ "CursorLine", "Current line of cursor
"				\ ]

exec "hi Cursor guifg=" . s:bg1 . " guibg=" . s:fg1 .
			\ " gui=none ctermfg=NONE ctermbg=NONE cterm=none term=none"

" Code Syntax
"	let s:synCode = [
"				\ "Normal", "Default text color
"				\ "Comment", "Code Comment
"				\ "MatchParen", "Matching parenthesis/bracket
"				\ ]

exec "hi Normal guifg=" . s:fg1 . " guibg=" . s:bg1 .
			\ " gui=none ctermfg=NONE ctermbg=NONE cterm=none term=none"

" General Editor
"	let s:synGeneral = [
"				\ "NonText", "Non-buffer characters, like @ at EOB
"				\ "SpecialKey", "listchars, like whitespace and EOL
"				\ "EndOfBuffer", "End of buffer, filled with ~
"				\ "VertSplit", "Vertical split bar
"				\ "Terminal", "Terminal
"				\ "Title", "wat
"				\ "Visual", "Visual mode selection
"				\ "VisualNOS", "Visual mode selection, outside of VIM
"				\ ]

" Chrome
"	let s:synChrome = [
				\ "ColorColumn", "Line max-length column
				\ "FoldColumn", "Column showing open and closed folds (foldcolumn)
				\ "SignColumn", "Column showing diff next to line number in files (+/-/!)
				\ "LineNr", "Line number
				\ "LineNrAbove", "Line number above (relativenumber)
				\ "LineNrBelow", "Line number above (relativenumber)
				\ "CursorLineNr", "Current line number
				\ ]

" Status Line
"	let s:synStatus = [
"				\ "StatusLine", "Status line: Active Window
"				\ "StatusLineTerm", "Status line: Active Window, Terminal
"				\ "StatusLineNC", "Status line: Inactive Window
"				\ "StatusLineTermNC", "Status line: Inactive Window, Terminal
"				\ ]

" Tab Line
"	let s:synTabs = [
"				\ "TabLine", "Tabs: Inactive Tab
"				\ "TabLine", "Tabs: Active Tab
"				\ "TabLineFill", "Tabs: Empty Tab Line
"				\ ]

" Messages and Prompts
"	let s:synMsgs = [
"				\ "ModeMsg", "Mode message (e.g., -- INSERT --)
"				\ "MoreMsg", "More message (e.g., --More--)
"				\ "Question", "Prompt message
"				\ "QuickFixLine", "Current quickfix item
"				\ "WildMenu", "Wild Menu current match
"				\ ]

" Errors
"	let s:synErrors = [
"				\ "ErrorMsg",
"				\ "WarningMsg",
"				\ ]

" Code Folding
"	let s:synFolding = [
"				\ "Conceal", "Conceal Char Foreground
"				\ "Folded", "Closed fold line
"				\ ]

" Popup Menu
"	let s:synPopup = [
"				\ "Pmenu", "Normal popup menu item color
"				\ "PmenuSel", "Selected popup menu item color
"				\ "PmenuSbar", "Popup menu Scrollbar
"				\ "PmenuThumb", "Scrollbar handle
"				\ ]

" Netrw
"	let s:synNetrw = [
"				\ "Directory", "Styling for directories, as opposed to files
"				\ ]

" Search
"	let s:synSearch = [
"				\ "IncSearch", "Search result preview
"				\ "Search", "Current search result
"				\ ]

" Spellchecker
"	let s:synSpellchk = [
"				\ "SpellBad", "Unrecognized word
"				\ "SpellCap", "Word that needs Capitalization
"				\ "SpellLocal", "Out of region word
"				\ "SpellRare", "Word that's hardly ever used
"				\ ]


" Diff Files
"	let s:synDiff = [
"				\ "DiffAdd",
"				\ "DiffChange",
"				\ "DiffDelete",
"				\ "DiffText",
"				\ ]


" folded
" bar
" baz

let s:inverted_items = [
	\ "Cursor",
	\ "CursorIM",
	\ "CursorColumn",
	\ "CursorLineNr",
	\ "Visual",
	\ "Search",
	\ "StatusLine"
	\ ]

let s:sub_inverted_items = [
	\ "StatusLineNC",
	\ "EndOfBuffer",
	\ "VertSplit",
	\ "PmenuSel",
	\ ]

let s:special_items = [
	\ "CursorLine",
	\ "ColorColumn",
	\ "EndOfBuffer",
	\ "NonText"
	\ ]

let s:error_items = [
	\ "SpellBad",
	\ "Error"
	\ ]

"for s:item in s:everything
"	exec "hi " . s:item . " guifg=" . s:fg . " guibg=" . s:bg .
"				\ " gui=none ctermfg=NONE ctermbg=NONE cterm=none term=none"
"endfor

"for s:item in s:syn1
"	exec "hi " . s:item . " guifg=" . s:fg1 . " guibg=" . s:bg .
"				\ " gui=none ctermfg=NONE ctermbg=NONE cterm=none term=none"
"endfor
"for s:item in s:syn2
"	exec "hi " . s:item . " guifg=" . s:fg2 . " guibg=" . s:bg .
"				\ " gui=none ctermfg=NONE ctermbg=NONE cterm=none term=none"
"endfor
"for s:item in s:syn3
"	exec "hi " . s:item . " guifg=" . s:fg3 . " guibg=" . s:bg .
"				\ " gui=none ctermfg=NONE ctermbg=NONE cterm=none term=none"
"endfor
"
"for s:item in s:inverted_items
"	exec "hi " . s:item . " guifg=" . s:bg . " guibg=" . s:fg
"endfor
"
"for s:item in s:sub_inverted_items
"	exec "hi " . s:item . " guifg=" . s:bg . " guibg=" . s:subbg
"endfor
"
"for s:item in s:special_items
"	exec "hi " . s:item . " guibg=" . s:special
"endfor
"
"for s:item in s:error_items
"	exec "hi " . s:item . " guifg=#ff0000"
"endfor

"hi! Search ctermbg=blue guibg=DarkBlue
""hi! ColorColumn ctermbg=0 guibg=DarkGray
"hi! SpecialKey ctermfg=0 guifg=DarkGray
"hi! NonText ctermfg=0 guifg=DarkGray
"
"hi! DiffAdd		guifg=#009900 ctermfg=2
"hi! DiffDelete guifg=#bbbb00 ctermfg=3
"hi! DiffChange guifg=#ff2222 ctermfg=1
"
"hi! LineNr ctermfg=242
"hi! CursorLineNr ctermfg=238
""
"" Change color of comment
""highlight Comment ctermfg=241 guifg=#FF0000
"highlight! link PreProc Comment
"highlight! link Special Comment
