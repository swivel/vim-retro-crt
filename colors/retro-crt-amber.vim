" =============================================================================
" Name: retro-crt-amber
" Inspired by vim-amber
" -----------------------------------------------------------------------------
"
" This is a theme that simulates the retro PC amber monochrome CRT.
" =============================================================================

hi clear
hi clear Normal
if exists('syntax on')
	syntax reset
endif

let colors_name='retro-crt-amber'

set background=dark

" Amber
let s:fg1 = ["#fc9505", 208, "black"]
let s:fg2 = ["#f08505", 208, "black"]
let s:fg3 = ["#d47e05", 172, "black"]
let s:fg4 = ["#ab6705", 130, "black"]
let s:fg5 = ["#5a3805", 58, "black"]

let s:bg5 = ["#4f4f45", 238, "black"]
let s:bg4 = ["#181810", 234, "black"]
let s:bg3 = ["#11110b", 233, "black"]
let s:bg2 = ["#0b0b06", 232, "black"]
let s:bg1 = ["#070703", 16, "black"]


let s:fgErr = ["#ee0005", 196, "black"]

let s:fg1 = s:fg2
let s:bg0 = ["#000000", 16, "black"]

let s:fg = s:fg3
let s:bg = s:bg1

let s:highlights = [
			\ [ "Normal", s:bg, s:fg, "NONE" ],
			\ [ "NonText", "bg", s:bg3, "NONE" ],
			\ [ "Comment", "bg", s:bg5, "NONE" ],
			\ [ "Conceal", "bg", s:bg5, "NONE" ],
			\ 
			\ [ "Constant", "bg", s:fg5, "NONE" ],
			\ [ "Character", "Constant" ],
			\ [ "Number", "Constant" ],
			\ [ "Float", "Number" ],
			\ [ "Boolean", "Constant" ],
			\ [ "String", "Constant" ],
			\ 
			\ [ "Identifier", "bg", s:fg1, "bold" ],
			\ [ "Function", "Identifier" ],
			\ 
			\ [ "Statement", "bg", s:fg5, "NONE" ],
			\ [ "Conditonal", "Statement" ],
			\ [ "Repeat", "Statement" ],
			\ [ "Label", "Statement" ],
			\ [ "Keyword", "Statement" ],
			\ [ "Exception", "Statement" ],
			\ 
			\ [ "Operator", "bg", s:fg3, "NONE" ],
			\ 
			\ [ "PreProc", "bg", s:fg5, "NONE" ],
			\ [ "Include", "PreProc" ],
			\ [ "Define", "PreProc" ],
			\ [ "Macro", "PreProc" ],
			\ [ "PreCondit", "PreProc" ],
			\ 
			\ [ "Type", "bg", s:fg5, "bold" ],
			\ [ "StorageClass", "Type" ],
			\ [ "Structure", "Type" ],
			\ [ "Typedef", "Type" ],
			\ 
			\ [ "Special", "NONE", s:fg5, "NONE" ],
			\ [ "SpecialChar", "Special" ],
			\ [ "Tag", "Special" ],
			\ [ "Delimiter", "Special" ],
			\ [ "SpecialComment", "Special" ],
			\ [ "Debug", "Special" ],
			\ 
			\ [ "Error", "NONE", s:fgErr, "bold" ],
			\ [ "ErrorMsg", "Error" ],
			\ [ "Warning", "NONE", s:fg1, "bold" ],
			\ [ "WarningMsg", "Warning" ],
			\ 
			\ [ "ModeMsg", "NONE", s:bg5, "NONE" ],
			\ [ "MoreMsg", "ModeMsg" ],
			\ [ "Question", "ModeMsg" ],
			\ 
			\ [ "Ignore", "NonText" ],
			\ [ "Todo", "NONE", s:fg1, "bold" ],
			\ [ "Underlined", "NONE", s:fg, "underline" ],
			\ 
			\ [ "StatusLine", s:bg2, s:fg, "NONE" ],
			\ [ "StatusLineNC", s:bg0, s:fg1, "NONE" ],
			\ [ "StatusLineTerm", "StatusLine" ],
			\ [ "StatusLineTermNC", "StatusLineNC" ],
			\ [ "TabLine", "StatusLineNC" ],
			\ [ "TabLineFill", "StatusLineNC" ],
			\ [ "TabLineSel", "StatusLine" ],
			\ [ "WildMenu", s:bg, s:fg, "NONE" ],
			\ [ "VertSplit", s:bg4, s:bg0, "NONE" ],
			\ 
			\ [ "Title", "NONE", s:fg, "bold" ],
			\ 
			\ [ "LineNr", s:bg0, s:bg5, "NONE" ],
			\ [ "CursorLineNr", s:bg2, s:fg4, "bold" ],
			\ [ "Cursor", s:bg, s:fg, "NONE" ],
			\ [ "CursorLine", s:bg2, "NONE", "NONE" ],
			\ [ "CursorColumn", "CursorLine" ],
			\ [ "ColorColumn", s:bg0, "NONE", "NONE" ],
			\ [ "SignColumn", s:bg0, s:bg5, "NONE" ],
			\ 
			\ [ "Visual", s:bg3, "NONE", "NONE" ],
			\ [ "VisualNOS", s:bg3, "NONE", "NONE" ],
			\ 
			\ [ "Pmenu", s:bg0, "NONE", "NONE" ],
			\ [ "PmenuSbar", s:bg, "NONE", "NONE" ],
			\ [ "PmenuSel", s:bg2, s:fg5, "NONE" ],
			\ [ "PmenuThumb", s:bg, "NONE", "NONE" ],
			\ 
			\ [ "FoldColumn", "NONE", s:fg, "NONE" ],
			\ [ "Folded", s:bg, s:bg5, "NONE" ],
			\ 
			\ [ "SpecialKey", "NONE", s:bg3, "NONE" ],
			\ [ "IncSearch", s:fg, s:bg, "NONE" ],
			\ [ "Search", s:fg, s:bg, "NONE" ],
			\ [ "Directory", "NONE", s:fg5, "bold" ],
			\ [ "MatchParen", s:bg5, s:fg1, "bold" ],
			\ 
			\ [ "SpellBad", "NONE", s:fgErr, "underline" ],
			\ [ "SpellCap", "NONE", s:fg, "underline" ],
			\ [ "SpellLocal", "NONE", s:fg, "underline" ],
			\ [ "SpellRare", "SpellLocal" ],
			\ 
			\ [ "QuickFixLine", s:bg, "NONE", "NONE" ],
			\ 
			\ [ "DiffAdd", s:bg2, s:fg, "NONE" ],
			\ [ "DiffChange", s:bg2, "NONE", "NONE" ],
			\ [ "DiffDelete", s:bg2, s:fgErr, "NONE" ],
			\ [ "DiffText", s:bg, s:fg, "NONE" ],
			\ [ "diffAdded", "DiffAdd" ],
			\ [ "diffRemoved", "DiffDelete" ],
			\ 
			\ [ "helpHyperTextJump", "bg", s:fg, "NONE" ],
			\ [ "htmlTag", "htmlTagName" ],
			\ [ "htmlEndTag", "htmlTag" ],
			\ [ "gitcommitSummary", "Title" ]
			\ ]

" Dark
	" black
	" red
	" green
	" yellow
	" blue
	" magenta
	" cyan
	" white
" Bright
"let s:terminal_ansi_colors = [
"			\ s:bg,
"			\ ["#ac2c2c"],
"			\ ["#4e9a06"],
"			\ ["#c4a000"],
"			\ ["#3465a4"],
"			\ ["#75507b"],
"			\ ["#389aad"],
"			\ s:bg,
"			\ 
"			\ s:bg5,
"			\ s:red,
"			\ s:green,
"			\ s:yellow,
"			\ ["#729fcf"],
"			\ s:purple,
"			\ ["#34e2e2"],
"			\ s:fg
"			\ ]

if ($TERM =~ '256' || &t_Co >= 256) || has("gui_running")
	for s:highlight in s:highlights
		if len(s:highlight) == 4
			exec "hi " . s:highlight[0] .
				\ " ctermbg=" . (type(s:highlight[1]) == type("") ? s:highlight[1] : s:highlight[1][1]) .
				\ " ctermfg=" . (type(s:highlight[2]) == type("") ? s:highlight[2] : s:highlight[2][1]) .
				\ " cterm=" . s:highlight[3] .
				\ " guibg=" . (type(s:highlight[1]) == type("") ? s:highlight[1] : s:highlight[1][0]) .
				\ " guifg=" . (type(s:highlight[2]) == type("") ? s:highlight[2] : s:highlight[2][0]) .
				\ " gui=" . s:highlight[3]
		elseif len(s:highlight) > 4
			exec "hi " . s:highlight[0] .
				\ " ctermbg=" . (type(s:highlight[1]) == type("") ? s:highlight[1] : s:highlight[1][1]) .
				\ " ctermfg=" . (type(s:highlight[2]) == type("") ? s:highlight[2] : s:highlight[2][1]) .
				\ " cterm=" . s:highlight[3] .
				\ " guibg=" . (type(s:highlight[1]) == type("") ? s:highlight[1] : s:highlight[1][0]) .
				\ " guifg=" . (type(s:highlight[2]) == type("") ? s:highlight[2] : s:highlight[2][0]) .
				\ " gui=" . s:highlight[3] .
				\ " guisp=" . (type(s:highlight[4]) == type("") ? s:highlight[4] : s:highlight[4][0])
		endif
	endfor

elseif &t_Co == 8 || $TERM !~# '^linux' || &t_Co == 16
	set t_Co=16

	for s:highlight in s:highlights
		if len(s:highlight) > 2
			exec "hi " . s:highlight[0] .
					\ " ctermbg=" . type(s:highlight[1]) == type("") ? s:highlight[1] : s:highlight[1][2] .
					\ " ctermfg=" . type(s:highlight[2]) == type("") ? s:highlight[2] : s:highlight[2][2] .
					\ " cterm=" . s:highlight[3]:
		endif
	endfor
endif

" Links
for s:link in s:highlights
	if len(s:link) == 2
		exec "hi! link " . s:link[0] . " " . s:link[1]
	endif
endfor

"if len(s:terminal_ansi_colors) == 16
"	let g:terminal_ansi_colors = s:terminal_ansi_colors
"endif
